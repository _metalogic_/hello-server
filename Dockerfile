# Go Builder Image
FROM golang:1.8-alpine AS builder

# set build arguments: GitHub user and repository
ARG GIT_SERVICE
ARG GIT_USER
ARG GIT_REPO

ENV git_service ${GIT_SERVICE:-github.com}
ENV git_user ${GIT_USER:-epbcdev}

# Create and set working directory
# Create and set working directory
RUN mkdir -p /go/src/$git_service/$GIT_REPO
WORKDIR /go/src/$git_service/$GIT_REPO

# copy sources
COPY . .

# Run tests, skip 'vendor'
RUN go test -v $(go list ./... | grep -v /vendor/)

# Build application
RUN CGO_ENABLED=0 go build -v -o "dist/myapp"

# ---
# Application Runtime Image
FROM alpine:3.6

# set build arguments: GitHub user and repository
ARG GIT_SERVICE
ARG GIT_USER
ARG GIT_REPO

ENV git_service ${GIT_SERVICE:-github.com}
ENV git_user ${GIT_USER:-epbcdev}

# copy file from builder image
COPY --from=builder /go/src/$git_service/$GIT_REPO/dist/myapp /usr/bin/myapp

EXPOSE 8080

CMD ["myapp", "--help"]

